#BATTLESHIP-APP
- Tools required for project usage:
    - NodeJS - https://nodejs.org/
    - CodeEditor (eg, WebStorm, VSCode, Atom)
    - GIT client (eg. https://git-scm.com)
    - Console/Terminal
    - Back-End server on link: https://bitbucket.org/ag04/mm-battleship-rest/src/master/
- clone project to your pc: git clone git@bitbucket.org:ag04/mm-battleships-app.git
- navigate inside project folder
- install all project dependencies with npm install
- in a browser open address http://localhost:3000
- Have fun! :)

###Packages used in projects
- redux: https://redux.js.org
- redux-saga - https://redux-saga.js.org
- reselect - https://github.com/reduxjs/reselect
- typesafe-actions - https://github.com/piotrwitek/typesafe-actions
- axios: https://github.com/axios/axios
- immer:  https://github.com/mweststrate/immer
