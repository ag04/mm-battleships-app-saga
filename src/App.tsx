import * as React from 'react';
import './App.css';
import BattleshipApp from './components/battleshipApp/BattleshipApp';
import { Switch, Route, RouteComponentProps, withRouter,  } from 'react-router';
import Players from './containers/Players/Players';
import NewPlayer from './containers/NewPlayer/NewPlayer';
import { connect } from 'react-redux';
import { playersActions } from './ducks/players/action';
import CreateGame from './containers/CreateGame/CreateGame';
import PlayerGames from './containers/PlayerGames/PlayerGames';
import ShowGameStatus from './containers/ShowGameStatus/ShowGameStatus';

type Props = typeof mapDispatchToProps & RouteComponentProps;

class App extends React.Component<Props> {
  componentDidMount(){
    this.props.fetchPlayers();
  }
  public render() {
    return (
      <>
        <Route path="/" component={BattleshipApp} />
        <Switch>
          <Route path = "/player/:id/game/list" component = {PlayerGames}/>
          <Route path ="/player/:playerId/game/:gameId" component = {ShowGameStatus}/>
          <Route path = "/player/create/game" component = {CreateGame}/>
          <Route path = "/player/list" component = {Players}/>
          <Route path ="/newplayer" component={NewPlayer} />
        </Switch>
      </>
    );
  }
}

const mapDispatchToProps = {
  fetchPlayers: playersActions.fetchPlayers,
};

export default withRouter(connect(null,mapDispatchToProps)(App));