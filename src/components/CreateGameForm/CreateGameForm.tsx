import * as React from 'react';
import { PlayerDto } from 'src/types/PlayerDto';
import styled from 'styled-components';

const LabelStyle = styled.label`
  font-weight: bold;
  display: block;
  margin-bottom: 8px;
`;

const SelectStyle = styled.select`
  outline: none;
  border: 1px solid #ccc;
  background-color: white;
  padding: 6px 10px;
  display: block;
  width: 100%;
  margin-top: 15px;
  margin-bottom: 15px;
  box-sizing: border-box;
`;

interface Props {
  labelName: string,
  players: PlayerDto []
  change: (event: React.ChangeEvent<HTMLSelectElement>) => void,
}

const CreatePlayerForm: React.FC<Props>= ({labelName, players, change}) => (
      <>
        <LabelStyle htmlFor="">{labelName}</LabelStyle>
        <SelectStyle onChange={change}>
        <option value = '' selected disabled hidden></option>
            {players.map((player, index)=>(
                <option key = {index} value={player.id}>{player.name}</option>
            ))}
        </SelectStyle>
      </>
);

export default CreatePlayerForm;
