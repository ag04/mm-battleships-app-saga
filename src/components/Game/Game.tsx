import * as React from 'react';

import { Link } from 'react-router-dom';
import styled from 'styled-components';

const GameStyle = styled.div`
  width: 250px;
  padding: 16px;
  color:#00796B;
  text-align: center;
  border: 1px solid #eee;
  box-shadow: 0 2px 3px #ccc;
  border-radius: 10px;
  margin: 25pt;
  box-sizing: border-box;
  font: 25pt;
  cursor: pointer;
  background-color: #E0F2F1;
`;

interface Props {
  playerId: number,
  gameId: number,
  opponentPlayer: string,
  status: string,
}

const Game: React.FC<Props> = ({playerId,gameId,status,opponentPlayer}) => (
  <Link to = {'/player/'+ playerId + '/game/'+ gameId}>    
    <GameStyle>
        <p>Game id: {gameId}</p>
        <p>Opponent: {opponentPlayer}</p>
        <p>status: {status}</p>
    </GameStyle>
  </Link>
);

export default Game;
