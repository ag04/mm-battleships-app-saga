import * as React from 'react';
import styled from 'styled-components';


const LeftObjectStyle = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  justify-content: left;
  position: fixed;
  width: 50%;
`;

const ParametarStyle = styled.div`
  text-align: left;
  font-size: 13pt;
  margin-top: 10pt;
`;

const PlayerTurnStyle = styled.div`
  width: 570px;
  height: 60px;
  padding: 16px;
  color:#00796B;
  text-align: center;
  border: 1px solid #eee;
  box-shadow: 0 2px 3px #ccc;
  border-radius: 10px;
  margin: 25pt;
  margin-top: 20pt;
  box-sizing: border-box;
  font: 25pt;
  background-color: #E0F2F1;
`;

const BlockStyle = styled.div`
  width: 250px;
  padding: 16px;
  color:#00796B;
  text-align: center;
  border: 1px solid #eee;
  box-shadow: 0 2px 3px #ccc;
  border-radius: 10px;
  margin: 25pt;
  box-sizing: border-box;
  font: 25pt;
  cursor: pointer;
  background-color: #E0F2F1;
`;

const ColonStyle = styled.div`
  width: 15px;
  height:15px;
  border: 1px solid #00796B;
`

interface Props {
  playerName: string,
  board: string[],
  remainingShips: number,
  autopilot: string,
  opponentName: string,
  opponentBoard: string[],
  opponentRemainingShips: number,
  opponentAutopilot: string,
  playerTurn: string,
  playerTurnName:string,
  checkIfDot: (colon: string) => string,
  onClickColon: (index1: number, index2: number) => void,
}

const GameStatus: React.FC<Props> = ({
  playerName,
  board,
  remainingShips,
  autopilot,
  opponentName,
  opponentBoard,
  opponentRemainingShips,
  opponentAutopilot,playerTurn,
  playerTurnName,
  checkIfDot,
  onClickColon}) =>(
  <>
    <LeftObjectStyle>
      <BlockStyle>
        <p>  Your username: {playerName}</p>
        <table>
          <tbody>
            {board.map((field, index1) => (
                  <tr key = {index1}> {field.split('').map((colon, index2)=> (
                      <td key = {index2} onClick = {() =>onClickColon(index1,index2)}>
                        <ColonStyle>
                          {checkIfDot(colon)}
                        </ColonStyle>
                      </td>
                      ))} 
                  </tr>
            ))}
          </tbody>
        </table>
        <ParametarStyle>remaining ships: {remainingShips}</ParametarStyle>
        <ParametarStyle>autopilot: {autopilot}</ParametarStyle>
      </BlockStyle>

      <BlockStyle>
        <p>  Opponent username: {opponentName}</p>
        <table>
          <tbody>
            {opponentBoard.map((field, index1) => (
              <tr key = {index1}> {field.split('').map((colon, index2)=> (
                  <td key = {index2} onClick = {(event) => onClickColon(index1,index2)}>
                    <ColonStyle>
                      {checkIfDot(colon)}
                    </ColonStyle>
                  </td>
                  ))} 
              </tr>
            ))}
          </tbody>
        </table>
        <ParametarStyle>remaining ships: {opponentRemainingShips}</ParametarStyle>
        <ParametarStyle>autopilot: {opponentAutopilot}</ParametarStyle>
      </BlockStyle>

      <PlayerTurnStyle>
        {playerTurn}: {playerTurnName}
      </PlayerTurnStyle>
      
  </LeftObjectStyle>
  </>
)


export default GameStatus;
