import * as React from 'react';
import styled from 'styled-components';

interface Props {
  labelName: string,
  value: string,
  change: (event: React.ChangeEvent<HTMLInputElement>) => void,
}

const LabelStyle = styled.label`
  font-weight: bold;
  display: block;
  margin-bottom: 8px;
`;

const InputStyle = styled.input`
  font-weight: bold;
  display: block;
  margin-bottom: 8px;
`;

const NewPlayerForm: React.FC<Props> = ({labelName, change, value}) => (
  <div>
    <LabelStyle>{labelName}</LabelStyle>
    <InputStyle type="text" onChange={change} value={value}/>
  </div>
);

export default NewPlayerForm;
