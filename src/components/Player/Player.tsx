import * as React from 'react';

import { Link } from 'react-router-dom';
import styled from 'styled-components';

const PlayerStyle = styled.div`
  width: 250px;
  padding: 16px;
  color:#00796B;
  text-align: center;
  border: 1px solid #eee;
  box-shadow: 0 2px 3px #ccc;
  border-radius: 10px;
  margin: 25pt;
  box-sizing: border-box;
  font: 25pt;
  cursor: pointer;
  background-color: #E0F2F1;
`;

interface Props {
    id: number,
    name: string,
}

const Player: React.FunctionComponent<Props> = (props) => ( 
  <Link to={`/player/${props.id}/game/list`}>
    <PlayerStyle>
      {props.id} | {props.name}
    </PlayerStyle> 
  </Link>
); 

export default Player;