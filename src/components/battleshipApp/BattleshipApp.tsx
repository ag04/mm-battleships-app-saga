import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';

import styled from 'styled-components';
import Logo from './logo/Logo'; 
import NavButton from './NavButton';

const BattleshipAppStyle = styled.header`
  display: flex;
  flex-direction: column;
  width: 100%;
  margin: 0;
  height: 20%;  
  text-align: center;   
  font-weight: bold;
  color: #009688;
  background-color:#80CBC4;
`;

const BattleshipGameStyle = styled.div`
  justify-content: space-between;
  display: flex;
  flex-direction: row;
  font-size: 36pt;
`;

const BattleshipNavigationStyle = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
`;

type Props = RouteComponentProps;

const buttons = [
    {name: 'PLAYERS', url: '/player/list'},
    {name: 'ADD NEW PLAYER', url: '/newplayer'},
    {name: 'CREATE GAME', url: '/player/create/game'},
];

const BattleshipApp: React.FC<Props> = ({location}) => (
  <BattleshipAppStyle>
    <BattleshipGameStyle>
      <Logo />
      <p>BATTLESHIP GAME</p>
      <Logo reversed/>
    </BattleshipGameStyle>
    
    <BattleshipNavigationStyle>
      {buttons.map(({name, url}) => (
        <NavButton key={url} name={name} url={url} selected={location.pathname === url}/>
      ))}
    </BattleshipNavigationStyle>
  </BattleshipAppStyle>
);

export default BattleshipApp;