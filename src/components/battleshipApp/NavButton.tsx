import * as React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

interface ButtonStyleProps {
  selected: boolean,
}

const ButtonStyle = styled.div<ButtonStyleProps>`
  width: 250px;
  padding: 16px;
  color: #E0F2F1;
  text-align: center;
  margin-left: 25%;
  border-radius: 10px;
  margin: 25pt;
  box-sizing: border-box;
  font: 25pt;
  cursor: pointer;
  background-color: ${({ selected }) => selected ? '#00838F' : '#00ACC1'};
`;

interface Props {
  url: string,
  name: string,
  selected?: boolean,
}

const NavButton: React.FC<Props> = ({url, name, selected = false}) => (
  <Link to={url} >
    <ButtonStyle selected={selected}>
      {name}
    </ButtonStyle>  
  </Link>
);

export default NavButton;