import * as React from 'react';

import styled from 'styled-components';
import image from './battleship.png';

interface LogoStyleProps {
    reversed: boolean,
}

const LogoStyle = styled.img<LogoStyleProps>`
  height: 100pt;
  width: 300pt;
  ${({ reversed }) => reversed ? 'transform: scaleX(-1);' : ''}
`;

interface Props {
    reversed?: boolean,
}

const Logo: React.FunctionComponent<Props> = ({ reversed = false }) => ( 
  <LogoStyle reversed={reversed} src={image} alt=""/>
); 

export default Logo;