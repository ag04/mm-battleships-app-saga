import * as React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';

import CreateGameForm from '../../components/CreateGameForm/CreateGameForm';
import { RootState } from 'src/ducks/store';
import { getAllPlayers } from 'src/ducks/players/selectors';
import { gamesActions } from 'src/ducks/games/action';

const FormStyle = styled.form`
  display:flex;
  flex-direction: column;
  width: 50%;
  justify-content: center;
  align-items: center;
  padding: 16px;
  color:#00796B;
  text-align: center;
  border: 1px solid #eee;
  box-shadow: 0 2px 3px #ccc;
  border-radius: 10px; 
  margin-top: 15px;
  margin-left:25%;
  box-sizing: border-box;
  font: 25pt;
  cursor: pointer;
  background-color: #E0F2F1;
`;

const CreateButtonStyle = styled.button`
  width: 80px;
  height: 30px;
  background-color: #00BCD4;
  color:white;
  border-radius: 10px;
`;

type Props = ReturnType<typeof mapStateToProps> & typeof mapDispatchToProps;

class CreateGame extends React.Component<Props>{

  state = {
    challenger: 0,
    opponent: 0,
  }

  onChallengerChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    this.setState({challenger: event.target.value});
  }

  onOpponentChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    this.setState({opponent: event.target.value}); 
  }

  onSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const {challenger, opponent} = this.state;
    this.props.createGame({challenger, opponent});
  }

  render(){
    return(
      <div >
        <FormStyle onSubmit={this.onSubmit}>
          <CreateGameForm 
            labelName = 'Challenger'
            change = {this.onChallengerChange}
            players = {this.props.allPlayers}
          />
  
          <CreateGameForm 
            labelName = 'Opponent'
            change = {this.onOpponentChange}
            players = {this.props.allPlayers}
          /> 

          <CreateButtonStyle> CREATE </CreateButtonStyle>

      </FormStyle>
            </div>
    );
  }
}

const mapStateToProps = (state: RootState) => ({
  allPlayers: getAllPlayers(state),
});

const mapDispatchToProps =  {
    createGame: gamesActions.createGame,
}

export default connect(mapStateToProps,mapDispatchToProps) (CreateGame);
