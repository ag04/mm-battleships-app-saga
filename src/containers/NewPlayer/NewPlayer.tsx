import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import styled from 'styled-components';

import NewPlayerForm from 'src/components/NewPlayerForm/NewPlayerForm';
import { playersActions } from 'src/ducks/players/action';

const FormStyle = styled.form`
  display:flex;
  flex-direction: column;
  width: 50%;
  justify-content: center;
  align-items: center;
  padding: 16px;
  color:#00796B;
  text-align: center;
  border: 1px solid #eee;
  box-shadow: 0 2px 3px #ccc;
  border-radius: 10px; 
  margin-top: 15px;
  margin-left:25%;
  box-sizing: border-box;
  font: 25pt;
  cursor: pointer;
  background-color: #E0F2F1;
`;

type Props = typeof mapDispatchToProps & RouteComponentProps;

interface State {
  name: string,
  email: string,
}

class NewPlayer extends React.Component <Props, State> {
  state = {
    name: '',
    email: '',
  }

  onUsernameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({name: event.target.value});
  }

  onEmailChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({email: event.target.value}); 
  }

  onSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const {name, email} = this.state;
    this.props.createPlayer({name,email});
    this.props.history.push('/player/list');
  }

  render(){
    const {name, email} = this.state;
    return (
      <FormStyle onSubmit={this.onSubmit}>
        <NewPlayerForm labelName='Username' change={this.onUsernameChange} value={name}/>
        <NewPlayerForm labelName='Email' change={this.onEmailChange} value={email}/>
        <button> CREATE </button>
      </FormStyle>
    );
  }
}

const mapDispatchToProps = {
  createPlayer: playersActions.createPlayer, 
}

export default connect(null, mapDispatchToProps)(NewPlayer);
