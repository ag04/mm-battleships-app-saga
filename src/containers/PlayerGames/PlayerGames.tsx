import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import styled from 'styled-components';

import { gamesActions } from 'src/ducks/games/action';
import { RootState } from 'src/ducks/store';
import { getPlayerGameList } from 'src/ducks/games/selectors';
import Game from 'src/components/Game/Game';
import { getPlayerById } from 'src/ducks/players/selectors';

const PlayerGamesStyle = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap-reverse;
`
const PlayersName = styled.p`
  display: flex;
  width: 100%; 
  height: 50pt;
  background-color: #00ACC1;
  color: #E0F2F1;
  font: 70px;
  margin-top: 0pt;;   
  font-weight: bold;
  align-items: center;
  justify-content: center;
`
type OwnProps = RouteComponentProps<{id: string}>;

type Props = OwnProps & typeof mapDispatchToProps & ReturnType<typeof mapStateToProps>;

class PlayerGames extends React.Component <Props> {
  componentDidMount(){
    this.props.getAllPlayerGames(Number(this.props.match.params.id));
  }

  render(){
    const { allGames, match, player } = this.props;
    const { id } = match.params;
    return(
      <>
        <PlayersName>PLAYERS'NAME: {player.name}</PlayersName> 
        <PlayerGamesStyle>
          {allGames.map(({ oponentName, status, game_id}) => (
          <Game key={game_id} playerId={Number(id)} opponentPlayer={oponentName} status={status} gameId={game_id}/>
          ))}
        </PlayerGamesStyle>
      </>
    );
  }
}  

const mapDispatchToProps = {
  getAllPlayerGames: gamesActions.fetchGames,
}

const mapStateToProps = (state: RootState, props: OwnProps) => {
  const playerId = Number(props.match.params.id);
  return {
    allGames: getPlayerGameList(state, {playerId}),
    player: getPlayerById(state, {playerId})
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(PlayerGames);