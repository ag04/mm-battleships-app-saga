import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import Player from '../../components/Player/Player';
import styled from 'styled-components';
import { RootState } from 'src/ducks/store';
import { playersActions } from '../../ducks/players/action';
import { getAllPlayers } from 'src/ducks/players/selectors';


const PlayersStyle = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap-reverse;
`
type Props = ReturnType<typeof mapStateToProps> & typeof mapDispatchToProps;

class Players extends React.Component<Props> {
  componentDidMount(){
    this.props.fetchPlayers();
  }

  render () {
    if (this.props.allPlayers) {
      return(
        <PlayersStyle>
          {this.props.allPlayers.map((player: any) => (
          <Link to={'/player/'+ player.id + '/game/list'}> 
            <Player 
              key={player.id} 
              id = {player.id} 
              name = {player.name} />
          </Link>  
          ))}               
        </PlayersStyle>
      );
    }
    return <p>Please add a player.</p>       
  }    
};

const mapStateToProps = (state: RootState) => ({
  allPlayers: getAllPlayers(state),
});

const mapDispatchToProps = {
  fetchPlayers: playersActions.fetchPlayers,
};

export default connect (mapStateToProps, mapDispatchToProps)(Players);