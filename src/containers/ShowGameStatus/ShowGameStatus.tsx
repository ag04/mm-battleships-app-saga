import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import GameStatus from '../../components/GameStatus/GameStatus';
import { gameBoardActions } from 'src/ducks/GameBoard/action';
import { RootState } from 'src/ducks/store';
import { getGameStatus } from 'src/ducks/GameBoard/selectors';
import { getPlayerById } from 'src/ducks/players/selectors';


const RightObjectStyle = styled.div`
  display: flex;
  width: 50%;
  margin-left: 50%;
  flex-direction: column;
  justify-content: right;
  position: fixed;
`;

const AutopilotStyle = styled.button`
  width: 250px;
  padding: 16px;
  color: #E0F2F1;
  text-align: center;
  border-radius: 10px;
  margin: 25pt;
  margin-left: 200px;
  box-sizing: border-box;
  font: 30pt;
  cursor: pointer;
  background-color: #00ACC1;    
`;

const FireButtonStyle = styled.button`
  width: 60px;
  height: 30px;
  background-color:#E0F2F1;
  color: #00796B;
  border-radius: 10px;
`
const FireShotBoxStyle = styled.div`
  height: 60px;
  width: 350px;
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 16px;
  color:#E0F2F1;
  text-align: center;
  border: 1px solid #eee;
  box-shadow: 0 2px 3px #ccc;
  border-radius: 10px;
  box-sizing: border-box;
  font: 25pt;
  cursor: pointer;
  margin: 25pt; 
  margin-left: 150px;
  background-color: #00BCD4; 
`
const FireShotsStyle = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  padding: 16px;
  text-align: center;
  border-radius: 10px;
  font: 25pt;
  cursor: pointer;
`

type OwnProps = RouteComponentProps<{playerId: string, gameId: string}>;

type Props = OwnProps & typeof mapDispatchToProps & ReturnType<typeof mapStateToProps>;

class ShowGameStatus extends React.Component <Props>{
  
  state = {
    opponentId: 0,
    playerTurnId: 0,
    selected: [],
    errorMessage: null,
    change: false,
  }

  componentDidMount(){
    const { playerId, gameId } = this.props.match.params;
    const numberPlayerId = Number(playerId);
    const numberGameId = Number(gameId);
    this.props.showGameStatus({ playerId: numberPlayerId, gameId: numberGameId});
  }

  onClickColon = (index1: number, index2: number) => {
    const alphabet = ["A","B","C","D","E","F","G","H","I","J"];
    const coordinate = index1 + 1+ 'x' + alphabet[index2]
    this.setState({ selected: [...this.state.selected, coordinate] })
    
  }

  componentDidUpdate(prevProps: any){
    const { playerId, gameId } = this.props.match.params;
    const numberPlayerId = Number(playerId);
    const numberGameId = Number(gameId);
    if(prevProps.match.url !== this.props.match.url){  
      this.props.showGameStatus({ playerId: numberPlayerId, gameId: numberGameId});
      this.setState({selected: []})
    }

    if(!prevProps){
      this.props.showGameStatus({ playerId: numberPlayerId, gameId: numberGameId});
    }
    
    if (prevProps.gameStatus.game.player_turn && prevProps.gameStatus.game.player_turn !== this.props.gameStatus.game.player_turn ) {
      this.props.showGameStatus({ playerId: numberPlayerId, gameId: numberGameId});
    }
  }

  checkIfDot = (colon: string)=>{
    if (colon === '.'){
        return '';
    }
    return colon;
  }

  write = () => {
    if (this.props.gameStatus.game.player_turn){
        return 'player turn'
    }
    return 'won' 
  }

  onClickFire = (event: any) => {
    event.preventDefault();

    if(this.props.gameStatus.self.autopilot === "true" || this.props.gameStatus.opponent.autopilot === "true"){
      this.setState({
          selected: []
      })
    }
    const { playerId, gameId } = this.props.match.params;
    if (this.state.selected.length === 5) {
      this.setState({errorMessage: null});
      this.props.fireShots({playerId: Number(playerId), gameId: Number(gameId), shots: this.state.selected })  
    }else if (this.state.selected.length < 5) {
      this.setState({errorMessage: "Need five shots"});
    }else if (this.state.selected.length > 5) {
      this.setState({errorMessage: " five shots max",selected: []});
    }
  }

  salvo = () => {
    if (this.props.gameStatus.game.player_turn === this.props.gameStatus.self.player_id){
      return(
        <FireShotBoxStyle>
          <p>{this.state.errorMessage}</p>
          <FireShotsStyle>{this.printSelected()}</FireShotsStyle>
          <FireButtonStyle onClick = {this.onClickFire} >FIRE</FireButtonStyle>
        </FireShotBoxStyle>
      );
    }
    return null;
  }

  printSelected = () => {
    return  this.state.selected + ' '
  }

  onClickAutopilot = () => {
    this.props.autopilot({playerId: Number(this.props.match.params.playerId), gameId: Number(this.props.match.params.gameId)})  
  }

  render(){
    return(
      <>
        <GameStatus 
          playerName = {this.props.player.name} 
          board = {this.props.gameStatus.self.board} 
          remainingShips = {this.props.gameStatus.self.remaining_ships} 
          autopilot = {this.props.gameStatus.self.autopilot} 
          playerTurn = {this.write()} 
          playerTurnName = {this.props.turnPlayer.name}
          opponentName = {this.props.opponentPlayer.name}
          opponentBoard = {this.props.gameStatus.opponent.board}
          opponentRemainingShips = {this.props.gameStatus.opponent.remaining_ships}
          opponentAutopilot = {this.props.gameStatus.opponent.autopilot}
          checkIfDot = {this.checkIfDot}
          onClickColon = {this.onClickColon}
        />

        <RightObjectStyle>
          <AutopilotStyle onClick = {this.onClickAutopilot} >AUTOPILOT</AutopilotStyle>

          <Link to = {'/player/' + this.props.gameStatus.opponent.player_id + '/game/' + this.props.match.params.gameId}>
          <AutopilotStyle> CHANGE PLAYER </AutopilotStyle>
          </Link>

          <Link to = '/player'>
            <AutopilotStyle> LEAVE GAME </AutopilotStyle>
          </Link>

            {this.salvo()}
            
          </RightObjectStyle>
      </>
    );
  }
}

const mapStateToProps = (state: RootState, props: OwnProps) => {
  const gameStatus = getGameStatus(state);
  let turn: number;
  if (gameStatus.game && gameStatus.game.player_turn) {
    turn = Number(gameStatus.game.player_turn);
  } else {
    turn = Number(gameStatus.game.won);
  }

  return {
  gameStatus,
  player: getPlayerById(state, {playerId: Number(props.match.params.playerId)}),
  opponentPlayer: getPlayerById(state, {playerId: Number(gameStatus.opponent.player_id)}),
  turnPlayer: getPlayerById(state, {playerId: turn}),
  }
};

const mapDispatchToProps =  {
  showGameStatus: gameBoardActions.showGameStatus,
  fireShots: gameBoardActions.fireShots,
  autopilot: gameBoardActions.autopilot,
}

export default connect(mapStateToProps,mapDispatchToProps) (ShowGameStatus);
      