
export const getPlayerIdFromProps = <P>(_: any, props: P & {playerId: number}):number => props.playerId;
