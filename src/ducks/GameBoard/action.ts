import { createStandardAction, ActionType } from 'typesafe-actions';
import { GameStatusDto } from 'src/types/GameStatusDto';

interface ShowGameStatusData{
  playerId: number,
  gameId: number,
}

interface FireShots{
  playerId: number,
  gameId: number,
  shots: string[]
}

export const gameBoardActions = {
  showGameStatus: createStandardAction('SHOW_GAME_STATUS')<ShowGameStatusData>(),
  setGameStatus: createStandardAction('SET_GAME_STATUS')<GameStatusDto, ShowGameStatusData>(),
  fireShots: createStandardAction('FIRE_SHOTS') <FireShots>(),
  autopilot: createStandardAction('AUTOPILOT')<ShowGameStatusData>(),
}

export type GameBoardAction = ActionType<typeof gameBoardActions>