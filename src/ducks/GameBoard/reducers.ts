import produce from 'immer';
import { getType } from 'typesafe-actions';

import { GameBoardAction, gameBoardActions } from './action';
import { GameStatusDto } from '../../types/GameStatusDto';

interface GameStatusState{
 gameStatus: GameStatusDto,
}

export const initialState: GameStatusState = {
  gameStatus:{
    game: {
      player_turn: 0
    },
    opponent:{
      player_id: 0,
      board: [],
      remaining_ships: 0,
      autopilot: 'false',
    },
    self: {
      player_id: 0,
      board: [],
      remaining_ships: 0,
      autopilot: 'false',
    }
  }  
}

export const gameBoardReducer = produce <GameStatusState, GameBoardAction>(
  (state,action) => {
    switch(action.type){
      case getType(gameBoardActions.setGameStatus): 
      if(action.payload) {
        state.gameStatus = action.payload;
      } else {
        state.gameStatus = action.payload;
      }
      return;
    }
  }, initialState
);