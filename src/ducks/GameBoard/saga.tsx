import { getType, ActionType } from 'typesafe-actions';

import { gameBoardActions } from "./action";
import { takeLatest, all, call, put } from 'redux-saga/effects';
import { api } from 'src/utils/api';
import { GameStatusDto } from 'src/types/GameStatusDto';

interface showGameStatusData {
  playerId: number,
  gameId: number,
}

const gameStatus = (data: showGameStatusData) => api.get(`/player/${data.playerId}/game/${data.gameId}`).then(result => result.data);

function* showGameStatus (action:ActionType <typeof gameBoardActions.showGameStatus>){
  try {
    const newGameStatus: GameStatusDto = yield call(gameStatus, action.payload);
   yield put(gameBoardActions.setGameStatus(newGameStatus, action.payload));
  } catch(error) {
    console.log(error);
  }
}

interface fireShotsData{
  playerId: number,
  gameId: number,
  shots: string[],
}
const fire = (data: fireShotsData) => api.put(`/player/${data.playerId}/game/${data.gameId}`, {salvo: data.shots});

function* fireShots (action: ActionType <typeof gameBoardActions.fireShots>){
  try {
    yield call(fire, action.payload);
    const newGameStatus: GameStatusDto = yield call(gameStatus, action.payload);
    yield put(gameBoardActions.setGameStatus(newGameStatus, action.payload));

  } catch(error) {
    console.log(error);
  }
}

const callAutopilot = (data: showGameStatusData) => api.put(`/player/${data.playerId}/game/${data.gameId}/autopilot`);

function* autopilot (action: ActionType <typeof gameBoardActions.autopilot>){
  try {
    yield call(callAutopilot, action.payload);
    const newGameStatus: GameStatusDto = yield call(gameStatus, action.payload);
    yield put(gameBoardActions.setGameStatus(newGameStatus, action.payload));

  } catch(error) {
    console.log(error);
  }
}

function* gameBoardSagaWatcher(){
  yield all([
    takeLatest(getType(gameBoardActions.showGameStatus),showGameStatus),  
    takeLatest(getType(gameBoardActions.fireShots),fireShots),
    takeLatest(getType(gameBoardActions.autopilot), autopilot)
  ])
}

export default gameBoardSagaWatcher;
