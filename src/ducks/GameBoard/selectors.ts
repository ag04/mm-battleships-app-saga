import { RootState } from '../store';

export const getGameStatus = (state: RootState) => state.gameBoard.gameStatus; 