import { createStandardAction, ActionType } from 'typesafe-actions';
import { GameDto } from 'src/types/GameDto';

interface NewGame{
  challenger: number,
  opponent: number
}

export const gamesActions = {
  fetchGames: createStandardAction('FETCH_GAMES')<number>(),
  setPlayerGames: createStandardAction('SET_PLAYER_GAMES')<GameDto[], number>(),
  createGame: createStandardAction('CREATE_GAME')<NewGame>(),
}

export type GamesAction = ActionType<typeof gamesActions>