import produce from 'immer';
import { getType } from 'typesafe-actions';

import { GamesAction, gamesActions } from './action';
import { GameDto } from 'src/types/GameDto';

interface GamesState{
  [playerId: number]: GameDto[],

}

export const initialState: GamesState = {}

export const gamesReducer = produce <GamesState, GamesAction>(
  (state,action) => {
    switch(action.type){
      case getType(gamesActions.setPlayerGames):
        if(action.payload.length) {
          state[action.meta] = action.payload;
        } else {
          state[action.meta] = [];
        }
      return;
    }
  }, initialState
);