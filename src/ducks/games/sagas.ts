import { gamesActions } from "./action";
import { takeLatest, all, call, put } from 'redux-saga/effects';
import { getType, ActionType } from 'typesafe-actions';
import { api } from 'src/utils/api';
import { GameDto } from 'src/types/GameDto';

const fetchAllGames = (playerId: number) => api.get(`player/${playerId}/game/list`).then(result => result.data);

function* getGames(action:ActionType <typeof gamesActions.fetchGames>){
  try {
    const allPlayerGames: GameDto[] = yield call(fetchAllGames, action.payload);
    yield put(gamesActions.setPlayerGames(allPlayerGames, action.payload));
  } catch (error) {
    
  }
}

interface createNewGameData {
  challenger: number,
  opponent: number,
}

const createNewGame = (data: createNewGameData) => api.post(`/player/${data.challenger}/game`, {player_id: data.opponent});

function* createGame (action:ActionType <typeof gamesActions.createGame>){
  try {
    yield call(createNewGame, action.payload);
  } catch(error) {
    console.log(error);
  }
}

function* gamesSagaWatcher(){
  yield all([
    takeLatest(getType(gamesActions.fetchGames),getGames),
    takeLatest(getType(gamesActions.createGame),createGame),
  ])
}

export default gamesSagaWatcher;
