import { RootState } from '../store';
import { createSelector } from 'reselect';
import { getPlayerIdFromProps } from '../FromPropsSelector';
import { getAllPlayers } from '../players/selectors';
import { Player } from 'src/types/Player';

export const getAllPlayerGames = (state: RootState) => state.games; 

export const getPlayerGameList = createSelector(
  getAllPlayerGames,
  getPlayerIdFromProps,
  getAllPlayers,
  (allGames, playerId,players) => {
    if (allGames[playerId] && players){
      return allGames[playerId].map(({ opponent_id, status, game_id}) => {
        const oponentName = players.find(({id}: Player) => opponent_id === id) || { name: 'noname' };
        return { status, game_id, oponentName: oponentName.name };
      });
    }
    return [];
  }
);
