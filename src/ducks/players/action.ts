import { ActionType, createStandardAction } from 'typesafe-actions';
import { PlayerDto } from 'src/types/PlayerDto';

interface NewPlayer{
  name: string,
  email:string,
}

export const playersActions = {
  fetchPlayers: createStandardAction('FETCH_PLAYERS')(),
  initPlayers: createStandardAction('INIT_PLAYERS')<PlayerDto[]>(),
  createPlayer: createStandardAction('CREATE_PLAYER')<NewPlayer>(),
};

export type PlayersAction = ActionType<typeof playersActions>
