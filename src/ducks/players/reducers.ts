import produce from 'immer';
import { getType } from 'typesafe-actions';

import { PlayersAction, playersActions } from './action';
import { Player } from 'src/types/Player';

interface PlayersState{
  allPlayers: Player[],
}

export const initialState: PlayersState = {
  allPlayers: [],
};

export const playersReducer = produce<PlayersState,PlayersAction> (
  (state,action) => {
    switch(action.type){
      case getType(playersActions.initPlayers):
        state.allPlayers = action.payload;
        return;
    }
  },
  initialState
);

export default playersReducer;
