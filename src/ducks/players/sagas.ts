import { takeLatest } from "redux-saga";
import { getType, ActionType } from 'typesafe-actions';
import { playersActions } from './action';
import { all, call, put } from 'redux-saga/effects';
import { api } from '../../utils/api';
import { PlayerDto } from 'src/types/PlayerDto';

const fetchAllPlayers = () => api.get('player/list').then(result => result.data.players);

function* getPlayers (){
 try{
  const allPlayers: PlayerDto[] = yield call(fetchAllPlayers);
  yield put(playersActions.initPlayers(allPlayers));
 } catch(error) {
   console.log(error);
 }
}

interface createNewPlayerData {
  name: string,
  email: string,
}

const createNewPlayer = (data: createNewPlayerData) => api.post('player/',data);

function* createPlayer (action:ActionType <typeof playersActions.createPlayer>){
  try {
    yield call(createNewPlayer, action.payload);
    yield put(playersActions.fetchPlayers());
  } catch(error) {
    console.log(error);
  }
}

function* playersSagaWatcher(){
  yield all([
    takeLatest(getType(playersActions.fetchPlayers), getPlayers),
    takeLatest(getType(playersActions.createPlayer), createPlayer)
  ])
}

export default playersSagaWatcher;