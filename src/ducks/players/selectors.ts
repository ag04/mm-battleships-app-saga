import { RootState } from '../store';
import { createSelector } from 'reselect';
import { getPlayerIdFromProps } from '../FromPropsSelector';

export const getAllPlayers = (state: RootState) => state.players.allPlayers;

export const getPlayerById = createSelector(
  getAllPlayers,
  getPlayerIdFromProps,
  (players, playerId) => players.find(({ id }) => id === playerId) || {id: 0, name: '/', email: '/'}
);
