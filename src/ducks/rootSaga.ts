import { all } from 'redux-saga/effects';

import playersSagaWatcher from './players/sagas';
import gamesSagaWatcher from './games/sagas';
import gameBoardSagaWatcher from './GameBoard/saga';

export default function* rootSaga() {
  yield all([
    playersSagaWatcher(),
    gamesSagaWatcher(),
    gameBoardSagaWatcher(),
  ]);
}
