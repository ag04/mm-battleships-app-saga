import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import reduxSaga from 'redux-saga';
import { StateType } from 'typesafe-actions';

import rootSaga from './rootSaga';
import { playersReducer } from './players/reducers';
import { gamesReducer } from './games/reducers';
import { gameBoardReducer } from './GameBoard/reducers';

const sagaMiddleware = reduxSaga();

const composeEnhancers =
  (window as any).__REDUX_DEVTOOLS_EXTENSION__ &&
  (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const rootReducer = combineReducers({
  players: playersReducer,
  games: gamesReducer,
  gameBoard: gameBoardReducer,
});

export type RootState = StateType<typeof rootReducer>;

const store = createStore(
  rootReducer,
  composeEnhancers(
    applyMiddleware(sagaMiddleware),
  ),
);  

sagaMiddleware.run(rootSaga);

export default store;
  