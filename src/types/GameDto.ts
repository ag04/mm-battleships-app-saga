export interface GameDto {
  game_id: number,
  opponent_id: number,
  status: string,
}