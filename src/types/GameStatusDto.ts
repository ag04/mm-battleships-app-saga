export interface GameStatusDto{
  game: {
    player_turn?: number
    won?: number
  },
  opponent:{
    player_id: number,
    board: string[],
    remaining_ships: number,
    autopilot: string,
  },
  self: {
    player_id: number,
    board: string[],
    remaining_ships: number,
    autopilot: string,
  }
}
