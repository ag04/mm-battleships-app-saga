export interface Player {
  email: string,
  id: number,
  name: string,
}