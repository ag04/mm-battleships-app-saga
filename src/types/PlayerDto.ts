export interface PlayerDto {
  email: string,
  id: number,
  name: string,
}